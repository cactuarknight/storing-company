#!/bin/bash

apt update
apt upgrade -y

apt install php7.0 php7.0-mysql php7.0-sqlite3 php-mbstring php7.0-mbstring php-gettext libapache2-mod-php7.0 php7.0-xml php7.0-zip php-mcrypt php-gd php-curl unzip -y
phpenmod pdo_mysql
phpenmod mcrypt
phpenmod mbstring
phpenmod gd
phpenmod curl
a2enmod rewrite

service apache2 restart
apt autoremove -y

mkdir /srv/
mkdir /srv/tank/


cd /srv/tank/
wget https://download.nextcloud.com/server/releases/nextcloud-12.0.4.zip
unzip nextcloud-12.0.4.zip


