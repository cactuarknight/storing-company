#!/bin/bash

account="no-reply" #set this field to set the mailbox. Configured options are error and no-reply
subject="Test" #set this field to set the subject.

#set the detials of the email.
details="";

from="$account@storingcompany.com.au"
to="error@storingcompany.com.au"

echo -e "Subject: $subject \r\n\r\n$details" | msmtp -a $account --debug --from=$from -t $to
