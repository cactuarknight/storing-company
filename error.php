<?php

include 'pageframe.php'; // Contains page frame
include 'body.php'; // Contains page content

function nostock() {
  Return '
  <div class="container-fluid">
    <div class="jumbotron">
      <p class="text-center">Sorry we are currently of stock</p>
      <p class="text-center">Please leave your contact details below and we\'ll update as soon as more stock is available</p>
      <form role="form">
        <div class="form-group">
          <label for="name"><span class="glyphicon glyphicon-user"></span> Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
        </div>
        <div class="form-group">
          <label for="email"><span class="glyphicon glyphicon-envelope"></span> Email</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
        </div>
        <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-send"></span> Send</button>
      </form>
    </div>
  </div>';
}

function pagedisplay() {
// Function to display page
  global $pagestart, $head, $bodystart, $bodyend, $pagend; // Import variables
  echo $pagestart . $head . $bodystart; // Page start and head area
  echo navbar() . nostock();
  echo $bodyend . $pagend; // End of page
}

pagedisplay(); // Displays page

?>